# Copyright (C) 2016, 2018, 2021, 2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libs3dstl.a
LIBNAME_SHARED = libs3dstl.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC = src/s3dstl.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then\
	     echo "$(LIBNAME)";\
	   else\
	     echo "$(LIBNAME_SHARED)";\
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libs3dstl.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libs3dstl.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then\
	   echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(S3D_VERSION) s3d; then\
	   echo "s3d $(S3D_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SSTL_VERSION) sstl; then\
	   echo "sstl $(SSTL_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -DS3DSTL_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S3D_VERSION@#$(S3D_VERSION)#g'\
	    -e 's#@SSTL_VERSION@#$(SSTL_VERSION)#g'\
	    s3dstl.pc.in > s3dstl.pc

s3dstl-local.pc: s3dstl.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S3D_VERSION@#$(S3D_VERSION)#g'\
	    -e 's#@SSTL_VERSION@#$(SSTL_VERSION)#g'\
	    s3dstl.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig/" s3dstl.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/s3dstl.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-3dstl" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/libs3dstl.so"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/s3dstl.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-3dstl/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-3dstl/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/s3dstl.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libs3dstl.o s3dstl.pc s3dstl-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Test
################################################################################
TEST_OBJ = src/test_s3dstl.o
TEST_DEP = src/test_s3dstl.d

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
S3DSTL_CFLAGS =  $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags s3dstl-local.pc)
S3DSTL_LIBS =  $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs s3dstl-local.pc)

build_tests: build_library src/test_s3dstl.d
	@$(MAKE) -fMakefile -fsrc/test_s3dstl.d test_s3dstl

test: build_tests
	@printf "%s " "test_s3dstl"
	@if ./test_s3dstl > /dev/null 2>&1; then \
	  printf "\033[1;32mOK\033[m\n"; \
	else \
	  printf "\033[1;31mError\033[m\n"; \
	fi

clean_test:
	rm -f test_s3dstl test_empty.stl

src/test_s3dstl.d: config.mk s3dstl-local.pc
	@$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) $(S3D_CFLAGS) $(S3DSTL_CFLAGS) $(SSTL_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

src/test_s3dstl.o: config.mk s3dstl-local.pc
	$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) $(S3D_CFLAGS) $(S3DSTL_CFLAGS) $(SSTL_CFLAGS) -c $(@:.o=.c) -o $@

test_s3dstl: src/test_s3dstl.o config.mk s3dstl-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(S3DSTL_LIBS) $(RSYS_LIBS) $(S3D_LIBS) $(SSTL_LIBS)
